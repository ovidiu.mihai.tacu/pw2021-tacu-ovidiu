package com.example.bookingsp.repository;

import com.example.bookingsp.models.ERole;
import com.example.bookingsp.models.Role;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface RoleRepository extends MongoRepository<Role, String> {
    Optional<Role> findByName(ERole name);
}
