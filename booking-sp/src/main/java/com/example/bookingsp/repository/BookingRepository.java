package com.example.bookingsp.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.bookingsp.models.Booking;

public interface BookingRepository extends MongoRepository<Booking, String> {
    List<Booking> findByTitleContaining(String title);
    List<Booking> findByPublished(boolean published);
}