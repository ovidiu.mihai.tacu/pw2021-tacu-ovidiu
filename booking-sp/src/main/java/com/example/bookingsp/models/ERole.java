package com.example.bookingsp.models;

public enum ERole {
    ROLE_USER,
    ROLE_SUPPORT,
    ROLE_ADMIN
}