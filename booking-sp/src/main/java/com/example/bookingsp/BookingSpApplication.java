package com.example.bookingsp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookingSpApplication {

    public static void main(String[] args) {
        SpringApplication.run(BookingSpApplication.class, args);
    }

}
