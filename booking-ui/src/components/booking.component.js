import React, { Component } from "react";
import BookingDataService from "../services/booking.service";
import {withNamespaces} from "react-i18next";
import i18n from "../i18n";

class Booking extends Component {
    constructor(props) {
        super(props);
        this.onChangeTitle = this.onChangeTitle.bind(this);
        this.onChangeDescription = this.onChangeDescription.bind(this);
        this.getBooking = this.getBooking.bind(this);
        this.updatePublished = this.updatePublished.bind(this);
        this.updateBooking = this.updateBooking.bind(this);
        this.deleteBooking = this.deleteBooking.bind(this);

        this.state = {
            currentBooking: {
                id: null,
                title: "",
                description: "",
                published: false
            },
            message: ""
        };
    }

    componentDidMount() {
        this.getBooking(this.props.match.params.id);
    }

    onChangeTitle(e) {
        const title = e.target.value;

        this.setState(function(prevState) {
            return {
                currentBooking: {
                    ...prevState.currentBooking,
                    title: title
                }
            };
        });
    }

    onChangeDescription(e) {
        const description = e.target.value;

        this.setState(prevState => ({
            currentBooking: {
                ...prevState.currentBooking,
                description: description
            }
        }));
    }

    getBooking(id) {
        BookingDataService.get(id)
            .then(response => {
                this.setState({
                    currentBooking: response.data
                });
                console.log(response.data);
            })
            .catch(e => {
                this.setState({
                    content:
                        (e.response &&
                            e.response.data &&
                            e.response.data.message) ||
                        e.message ||
                        e.toString()
                });
            });
    }

    updatePublished(status) {
        var data = {
            id: this.state.currentBooking.id,
            title: this.state.currentBooking.title,
            description: this.state.currentBooking.description,
            published: status
        };

        BookingDataService.update(this.state.currentBooking.id, data)
            .then(response => {
                this.setState(prevState => ({
                    currentBooking: {
                        ...prevState.currentBooking,
                        published: status
                    }
                }));
                console.log(response.data);
            })
            .catch(e => {
                this.setState({
                    content:
                        (e.response &&
                            e.response.data &&
                            e.response.data.message) ||
                        e.message ||
                        e.toString()
                });
            });
    }

    updateBooking() {
        BookingDataService.update(
            this.state.currentBooking.id,
            this.state.currentBooking
        )
            .then(response => {
                console.log(response.data);
                this.setState({
                    message: "The booking was updated successfully!"
                });
            })
            .catch(e => {
                this.setState({
                    content:
                        (e.response &&
                            e.response.data &&
                            e.response.data.message) ||
                        e.message ||
                        e.toString()
                });
            });
    }

    deleteBooking() {
        BookingDataService.delete(this.state.currentBooking.id)
            .then(response => {
                console.log(response.data);
                this.props.history.push('/bookings')
            })
            .catch(e => {
                this.setState({
                    content:
                        (e.response &&
                            e.response.data &&
                            e.response.data.message) ||
                        e.message ||
                        e.toString()
                });
            });
    }

    render() {
        const { t } = this.props;
        const changeLanguage = (lng) => {
            i18n.changeLanguage(lng);
        }

        const { currentBooking } = this.state;

        return (
            <div>
                {currentBooking ? (
                    <div className="edit-form">
                        <h4>{t('Booking')}</h4>
                        <form>
                            <div className="form-group">
                                <label htmlFor="title">{t('Title')}</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="title"
                                    value={currentBooking.title}
                                    onChange={this.onChangeTitle}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="description">{t('Description')}</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="description"
                                    value={currentBooking.description}
                                    onChange={this.onChangeDescription}
                                />
                            </div>

                            <div className="form-group">
                                <label>
                                    <strong>{t('Status')}:</strong>
                                </label>
                                {currentBooking.published ? t('Published') : t('Pending')}
                            </div>
                        </form>

                        {currentBooking.published ? (
                            <button
                                className="badge badge-primary mr-2"
                                onClick={() => this.updatePublished(false)}
                            >
                                {t('UnPublish')}
                            </button>
                        ) : (
                            <buttonv
                                className="badge badge-primary mr-2"
                                onClick={() => this.updatePublished(true)}
                            >
                                {t('Publish')}
                            </buttonv>
                        )}

                        <button
                            className="badge badge-danger mr-2"
                            onClick={this.deleteBooking}
                        >
                            {t('Delete')}
                        </button>

                        <button
                            type="submit"
                            className="badge badge-success"
                            onClick={this.updateBooking}
                        >
                            {t('Update')}
                        </button>
                        <p>{this.state.message}</p>
                    </div>
                ) : (
                    <div>
                        <br />
                        <p>{t('ClickRequire')}</p>
                    </div>
                )}
            </div>
        );
    }
}

export default withNamespaces()(Booking);