import React, { Component } from "react";

import UserService from "../services/user.service";
import logo from '../img/travel.jpg'
import {withNamespaces} from "react-i18next";
import i18n from "../i18n";

class Home extends Component {
    constructor(props) {
        super(props);

        this.state = {
            content: ""
        };
    }

    componentDidMount() {
        UserService.getPublicContent().then(
            response => {
                this.setState({
                    content: response.data
                });
            },
            error => {
                this.setState({
                    content:
                        (error.response && error.response.data) ||
                        error.message ||
                        error.toString()
                });
            }
        );
    }

    render() {

        const { t } = this.props;
        const changeLanguage = (lng) => {
            i18n.changeLanguage(lng);
        }

        return (
            <div className="container">
                <header className="jumbotron">
                    <h3>{t('AppName')}</h3>
                </header>
                <img src={logo} alt="Example3" width="100%" height="500" />
            </div>
        );
    }
}

export default withNamespaces()(Home);