import React, { Component } from "react";
import BookingDataService from "../services/booking.service";
import AuthService from "../services/auth.service";

import { Link } from "react-router-dom";
import {withNamespaces} from "react-i18next";
import i18n from "../i18n";

class BookingsListNotPublished extends Component {
    constructor(props) {
        super(props);
        this.onChangeSearchTitle = this.onChangeSearchTitle.bind(this);
        this.retrieveBookings = this.retrieveBookings.bind(this);
        this.refreshList = this.refreshList.bind(this);
        this.setActiveBooking = this.setActiveBooking.bind(this);
        this.removeAllBookings = this.removeAllBookings.bind(this);
        this.searchTitle = this.searchTitle.bind(this);

        this.state = {
            bookings: [],
            currentBooking: null,
            currentIndex: -1,
            searchTitle: "",
            showModeratorBoard: false,
            showAdminBoard: false,
            currentUser: undefined,
        };
    }

    componentDidMount() {
        const user = AuthService.getCurrentUser();
        this.retrieveBookings();

        if (user) {
            this.setState({
                currentUser: user,
                showSupportBoard: user.roles.includes("ROLE_SUPPORT"),
                showAdminBoard: user.roles.includes("ROLE_ADMIN"),
            });
        }
    }

    onChangeSearchTitle(e) {
        const searchTitle = e.target.value;

        this.setState({
            searchTitle: searchTitle
        });
    }

    retrieveBookings() {
        BookingDataService.getNotPublished()
            .then(response => {
                this.setState({
                    bookings: response.data
                });
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }

    refreshList() {
        this.retrieveBookings();
        this.setState({
            currentBooking: null,
            currentIndex: -1
        });
    }

    setActiveBooking(booking, index) {
        this.setState({
            currentBooking: booking,
            currentIndex: index
        });
    }

    removeAllBookings() {
        BookingDataService.deleteAll()
            .then(response => {
                console.log(response.data);
                this.refreshList();
            })
            .catch(e => {
                console.log(e);
            });
    }

    searchTitle() {
        this.setState({
            currentBooking: null,
            currentIndex: -1
        });

        BookingDataService.findByTitle(this.state.searchTitle)
            .then(response => {
                this.setState({
                    bookings: response.data
                });
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }

    render() {
        const { t } = this.props;
        const changeLanguage = (lng) => {
            i18n.changeLanguage(lng);
        }

        const { searchTitle, bookings, currentBooking, currentIndex,
            currentUser, showSupportBoard, showAdminBoard } = this.state;

        return (
            <div className="list row">
                <div className="col-md-8">
                    <div className="input-group mb-3">
                        <input
                            type="text"
                            className="form-control"
                            placeholder="Search by title"
                            value={searchTitle}
                            onChange={this.onChangeSearchTitle}
                        />
                        <div className="input-group-append">
                            <button
                                className="btn btn-outline-secondary"
                                type="button"
                                onClick={this.searchTitle}
                            >
                                {t('Search')}
                            </button>
                        </div>
                    </div>
                </div>
                <div className="col-md-6">
                    <h4>{t('Pending Bookings List')}</h4>

                    <ul className="list-group">
                        {bookings &&
                        bookings.map((booking, index) => (
                            <li
                                className={
                                    "list-group-item " +
                                    (index === currentIndex ? "active" : "")
                                }
                                onClick={() => this.setActiveBooking(booking, index)}
                                key={index}
                            >
                                {booking.title}
                            </li>
                        ))}
                    </ul>

                    {(showAdminBoard || showSupportBoard) && (
                        <button
                            className="m-3 btn btn-sm btn-danger"
                            onClick={this.removeAllBookings}
                        >
                            {t('Remove All')}
                        </button>
                    )}

                </div>
                <div className="col-md-6">
                    {currentBooking ? (
                        <div>
                            <h4>{t('Booking')}</h4>
                            <div>
                                <label>
                                    <strong>{t('Title')}:</strong>
                                </label>{" "}
                                {currentBooking.title}
                            </div>
                            <div>
                                <label>
                                    <strong>{t('Description')}:</strong>
                                </label>{" "}
                                {currentBooking.description}
                            </div>
                            <div>
                                <label>
                                    <strong>{t('Status')}:</strong>
                                </label>{" "}
                                {currentBooking.published ? t('Published') : t('Pending')}
                            </div>

                            {(showAdminBoard || showSupportBoard) && (
                            <Link
                                to={"/bookings/" + currentBooking.id}
                                className="badge badge-warning"
                            >
                                {t('Edit')}
                            </Link>
                            )}

                        </div>
                    ) : (
                        <div>
                            <br />
                            <p>{t('ClickRequire')}</p>
                        </div>
                    )}
                </div>
            </div>
        );
    }
}

export default withNamespaces()(BookingsListNotPublished);