import React, { Component } from "react";
import BookingDataService from "../services/booking.service";
import {withNamespaces} from "react-i18next";
import i18n from "../i18n";

class AddBooking extends Component {
    constructor(props) {
        super(props);
        this.onChangeTitle = this.onChangeTitle.bind(this);
        this.onChangeDescription = this.onChangeDescription.bind(this);
        this.saveBooking = this.saveBooking.bind(this);
        this.newBooking = this.newBooking.bind(this);

        this.state = {
            id: null,
            title: "",
            description: "",
            published: false,

            submitted: false
        };
    }

    onChangeTitle(e) {
        this.setState({
            title: e.target.value
        });
    }

    onChangeDescription(e) {
        this.setState({
            description: e.target.value
        });
    }

    saveBooking() {
        var data = {
            title: this.state.title,
            description: this.state.description
        };

        BookingDataService.create(data)
            .then(response => {
                this.setState({
                    id: response.data.id,
                    title: response.data.title,
                    description: response.data.description,
                    published: response.data.published,

                    submitted: true
                });
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }

    newBooking() {
        this.setState({
            id: null,
            title: "",
            description: "",
            published: false,

            submitted: false
        });
    }

    render() {
        const { t } = this.props;
        const changeLanguage = (lng) => {
            i18n.changeLanguage(lng);
        }

        return (
            <div className="submit-form">
                {this.state.submitted ? (
                    <div>
                        <h4>{t('You submitted successfully!')}</h4>
                        <button className="btn btn-success" onClick={this.newBooking}>
                            {t('Add')}
                        </button>
                    </div>
                ) : (
                    <div>
                        <div className="form-group">
                            <label htmlFor="title">Title</label>
                            <input
                                type="text"
                                className="form-control"
                                id="title"
                                required
                                value={this.state.title}
                                onChange={this.onChangeTitle}
                                name="title"
                            />
                        </div>

                        <div className="form-group">
                            <label htmlFor="description">Description</label>
                            <input
                                type="text"
                                className="form-control"
                                id="description"
                                required
                                value={this.state.description}
                                onChange={this.onChangeDescription}
                                name="description"
                            />
                        </div>

                        <button onClick={this.saveBooking} className="btn btn-success">
                            {t('Submit')}
                        </button>
                    </div>
                )}
            </div>
        );
    }
}

export default withNamespaces()(AddBooking);