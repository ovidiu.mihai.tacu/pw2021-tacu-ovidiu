import React, { Component } from "react";
import { Switch, Route, Link } from "react-router-dom";
import { withNamespaces } from 'react-i18next';
import i18n from './i18n';

import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

import AuthService from "./services/auth.service";

import Login from "./components/login.component";
import Register from "./components/register.component";
import Home from "./components/home.component";
import Profile from "./components/profile.component";
import BoardUser from "./components/board-user.component";
import BoardSupport from "./components/board-support.component";
import BoardAdmin from "./components/board-admin.component";
import AddBooking from "./components/add-booking.component";
import Booking from "./components/booking.component";
import BookingsListPublished from "./components/bookings-list-published.component";
import BookingsListNotPublished from "./components/bookings-list-notPublished.component";


class App extends Component {
  constructor(props) {
    super(props);
    this.logOut = this.logOut.bind(this);

    this.state = {
      showModeratorBoard: false,
      showAdminBoard: false,
      currentUser: undefined,
    };
  }

  componentDidMount() {
    const user = AuthService.getCurrentUser();

    if (user) {
      this.setState({
        currentUser: user,
        showSupportBoard: user.roles.includes("ROLE_SUPPORT"),
        showAdminBoard: user.roles.includes("ROLE_ADMIN"),
      });
    }
  }

  logOut() {
    AuthService.logout();
  }

  render() {
    const { currentUser, showSupportBoard, showAdminBoard } = this.state;
    const { t } = this.props;
    const changeLanguage = (lng) => {
      i18n.changeLanguage(lng);
    }

    return (
        <div>
          <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <Link to={"/home"} className="navbar-brand">
              {t('AppName')}
            </Link>
            <div className="navbar-nav mr-auto">
              <li className="nav-item">
                <Link to={"/bookings"} className="nav-link">
                  {t('Bookings')}
                </Link>
              </li>

              {currentUser && (
                  <li className="nav-item">
                    <Link to={"/add"} className="nav-link">
                      {t('AddBooking')}
                    </Link>
                  </li>
              )}

              {showSupportBoard && (
                  <li className="nav-item">
                    <Link to={"/bookings-pending"} className="nav-link">
                      {t('Support Board')}
                    </Link>
                  </li>
              )}

              {showAdminBoard && (
                  <li className="nav-item">
                    <Link to={"/admin"} className="nav-link">
                      {t('Admin Board')}
                    </Link>
                  </li>
              )}

            </div>


            {currentUser ? (
                <div className="navbar-nav ml-auto">
                  <li className="nav-item">
                    <Link to={"/profile"} className="nav-link">
                      {currentUser.username}
                    </Link>
                  </li>
                  <li className="nav-item">
                    <a href="/login" className="nav-link" onClick={this.logOut}>
                      {t('LogOut')}
                    </a>
                  </li>
                </div>
            ) : (
                <div className="navbar-nav ml-auto">
                  <li className="nav-item">
                    <Link to={"/login"} className="nav-link">
                      {t('LogIn')}
                    </Link>
                  </li>

                  <li className="nav-item">
                    <Link to={"/register"} className="nav-link">
                      {t('Sign Up')}
                    </Link>
                  </li>


                </div>
            )}

            <button type="button" className="btn btn-warning" onClick={() => changeLanguage('ro')}>ro</button>
            <button type="button" className="btn btn-info" onClick={() => changeLanguage('en')}>en</button>

          </nav>

          <div className="container mt-3">
            <Switch>
              <Route exact path={["/", "/home"]} component={Home} />
              <Route exact path="/login" component={Login} />
              <Route exact path="/register" component={Register} />
              <Route exact path="/profile" component={Profile} />
              <Route path="/user" component={BoardUser} />
              <Route path="/support" component={BoardSupport} />
              <Route path="/admin" component={BoardAdmin} />
              <Route exact path={["/", "/bookings"]} component={BookingsListPublished} />
              <Route exact path={["/", "/bookings-pending"]} component={BookingsListNotPublished} />
              <Route exact path="/add" component={AddBooking} />
              <Route path="/bookings/:id" component={Booking} />
            </Switch>
          </div>
        </div>
    );
  }
}

export default withNamespaces()(App);