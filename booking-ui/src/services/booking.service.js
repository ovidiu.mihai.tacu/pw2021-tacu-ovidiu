import http from "../http-common";

class BookingDataService {
    getAll() {
        return http.get("/bookings");
    }

    getPublished() {
        return http.get("/bookings/published");
    }

    getNotPublished() {
        return http.get("/bookings/notPublished");
    }

    get(id) {
        return http.get(`/bookings/${id}`);
    }

    create(data) {
        return http.post("/bookings", data);
    }

    update(id, data) {
        return http.put(`/bookings/${id}`, data);
    }

    delete(id) {
        return http.delete(`/bookings/${id}`);
    }

    deleteAll() {
        return http.delete(`/bookings`);
    }

    findByTitle(title) {
        return http.get(`/bookings?title=${title}`);
    }
}

export default new BookingDataService();